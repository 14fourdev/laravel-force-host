<?php namespace Tests\Unit;

use FourteenFour\ForceHost\Middleware\ForceHost;
use Illuminate\Http\Request;
use Tests\BaseTestCase;

class ForceHostMiddlewareTest extends BaseTestCase {

    public function test_redirect_route()
    {
        $request = Request::create('/', 'GET');

        $middleware = new ForceHost;

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response->getStatusCode(), 302);
    }

}
