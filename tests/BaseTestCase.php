<?php namespace Tests;

use Orchestra\Testbench\TestCase;

class BaseTestCase extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Return the base testing directory
     * @return string Base Testing Directory
     */
    public function test_path()
    {
        return __DIR__;
    }

    protected function getPackageProviders($app)
    {
        return [
            'FourteenFour\ForceHost\ForceHostServiceProvider',
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
    // Setup default database to use sqlite :memory:
        $app['config']->set('forcehost.enable', true);
    }

}
