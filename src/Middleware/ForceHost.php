<?php

namespace FourteenFour\ForceHost\Middleware;

use Closure;

class ForceHost
{

    /**
     * Handle requests in middleware to force the host header attribute.
     *
     * @param Illuminate\Http\Request $request The request object for processing
     * @param Closure $next The next step in the middleware process
     * @return Closure,redirect
     */
    public function handle($request, Closure $next)
    {
        $url = $request->fullUrl();
        $ignored = $this->checkIfIgnored($url);

        if(!config('forcehost.enable') || $ignored){
            return $next($request);
        }

        $request_host = $request->header('Host');
        $required_host = config('forcehost.host');

        if($request_host != $required_host){
            $request->headers->set('Host', $required_host);
        }

        return $next($request);
    }

    /**
     * Check if the URL is ignored in the configuration of the package
     *
     * @param string $url The requested URL
     * @return boolean
     */
    protected function checkIfIgnored($url)
    {
        $except = config('forcehost.except');

        $ignorable = false;

        foreach($except as $ignore) {
            if (preg_match("/$ignore/i", $url)) {
                $ignorable = true;
            }
        }

        return $ignorable;
    }

}
