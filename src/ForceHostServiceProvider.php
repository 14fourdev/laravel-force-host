<?php
namespace FourteenFour\ForceHost;

use Illuminate\Support\ServiceProvider;

/**
 * Force Host service provider. Register package into service provider
 */
class ForceHostServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/forcehost.php', 'forcehost');
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/forcehost.php' => config_path('forcehost.php'),
        ]);
    }

}
