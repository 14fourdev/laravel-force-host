<?php

return [


    /*
    |--------------------------------------------------------------------------
    | ENABLE FORCE HOST
    |--------------------------------------------------------------------------
    |
    | Enables the the Force Host middleware. This can be used to disable the
    | middleware. Use the environment variable FORCE_HOST to configure.
    |
    */
    'enable' => env('FORCE_HOST', true),



    /*
    |--------------------------------------------------------------------------
    | SET FORCED HOST
    |--------------------------------------------------------------------------
    |
    | The host specified for each request. In the event that the host doesn't
    | match the one specified here, we will update the host header attribute to
    | match the one here.
    |
    */
    'host' => env('FORCE_HOST_DOMAIN', 'localhost'),


    /*
    |--------------------------------------------------------------------------
    | SET EXCEPTION PATTERNS
    |--------------------------------------------------------------------------
    |
    | Set any regex patterns that you want to exclude from the Force Host
    | urls. Warning this checks the Full URL so query params are excluded
    | as well as domains (eg '14four.com' would match www.14four.com and
    | example.com/post?14four.com)
    |
    */
    'except' => [],

];
