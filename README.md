# Force Host for Laravel

## Installation

##### 1. Require Laravel Force Host with composer.

```
composer require 14four/laravel-force-host
```

##### 2. Open your config/app.php and add the following to the providers array:

*note: Laravel > 5.5 skip this step*

```php
FourteenFour\ForceHost\ForceHostServiceProvider::class,
```

##### 3. Publish Config

```
php artisan vendor:publish --provider="FourteenFour\ForceHost\ForceHostServiceProvider"
```

##### 4. Add middleware to kernel.php

```php
\FourteenFour\ForceHost\Middleware\ForceHost::class,
```

*note: this should probably be added to the 'web' middleware*

## Config

Open the `config/forcehost.php` configuration file to customize the config.

### Enable

`enable` will allow you to turn on and off the Force Host functionality of the middleware.

By default this uses the environment variable `FORCE_HOST`.

Use the following to disable the Force Host functionality.

```
FORCE_HOST=false
```

### Host

`host` is used to specify the domain in which you intend to force on the host header attribute via the Force Host middleware.

By default this uses the environment variable `FORCE_HOST_DOMAIN`.

Use the following to change the host you intend to force.

```
FORCE_HOST_DOMAIN=www.DoritosMadden19.com
```

### Except

This will allow you to provide regex patterns that will be excluded from the Force Host functionality.

Example:
```php
$except = [
  '14four.com',
],
```

Keep in mind this will match the _fullUrl_ not just the domain. The example above will match `http://14four.com` and `http://example.com/post?14four.com`
